<?php 

require("animal.php");
require("frog.php");
require("ape.php");

$animal = new Animal("shaun");
echo "name = ". $animal->name . "<br>";
echo "legs = ". $animal->legs . "<br>";
echo "cold blooded = ". $animal->coldbloded . "<br><br>";


$frog = new Frog("buduk");
echo $frog->jump();

$ape = new Ape("kera sakti");

echo $ape->yell();

 ?>